package controle;
import modelo.Usuario;
import java.sql.PreparedStatement; // Executar comandos SQL
import java.sql.ResultSet; // Armazenar dados captados da tabela
import java.sql.SQLException; // Gereciar erros em SQL
import controle.Conexao; // Efetuar conexão com o banco e fechar.
import java.util.ArrayList;
public class UsuarioC{
        public boolean removerUsuario(int id){
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM usuario WHERE id=?;");
            ps.setInt(1,id);
            if(!ps.execute()){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            System.out.println("[-] Não foi possível remover as informações do usuário.");
            System.out.println(e.getMessage());
            return false;
        }finally{
            con.fecharConexao();
        }
    }

    public ArrayList<Usuario> selecionarTodos(){
        ArrayList<Usuario> lista = null;
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM usuario;");
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                lista = new ArrayList<Usuario>();
                while(rs.next()){
                    Usuario user = new Usuario();
                    user.setId(rs.getInt("id"));
                    user.setNome(rs.getString("nome"));
                    user.setSenha(rs.getString("senha"));
                    lista.add(user);
                }
            }
        }catch(SQLException e){
            System.out.println("[-] Erro ao selecionar usuários.");
            System.out.println(e.getMessage());
        }finally{
            con.fecharConexao();
            return lista;
        }
    }
    public Usuario selecionarUm(int id){
        Usuario user = null;
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM usuario WHERE id=?;");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                user = new Usuario();
                user.setId(rs.getInt("id"));
                user.setNome(rs.getString("nome"));
                user.setSenha(rs.getString("senha"));
            }
        }catch(SQLException e){
            System.out.println("[-] Erro ao selecionar usuários.");
            System.out.println(e.getMessage());
        }finally{
            con.fecharConexao();
            return user;
        }
    }
    public boolean editarUsuario(Usuario user){
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("UPDATE usuario SET nome=?,senha=? WHERE id=?;");
            ps.setString(1,user.getNome());
            ps.setString(2,user.getSenha());
            ps.setInt(3,user.getId());
            if(!ps.execute()){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            System.out.println("[-] Não foi possível editar as informações do usuário.");
            System.out.println(e.getMessage());
            return false;
        }finally{
            con.fecharConexao();
        }
    }
    public boolean login(Usuario user){
        Conexao con = new Conexao();
        try{    
            // Abrindo a conexão
            con.abrirConexao();
            // Realizar ações
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM usuario WHERE nome=? AND senha=?;");
            ps.setString(1,user.getNome());
            ps.setString(2,user.getSenha());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            System.out.println("Erro ao consultar o banco");
            return false;
        }finally{
            con.fecharConexao();
        }
    }
    public boolean cadastrar(Usuario user){
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO usuario (nome,senha) VALUES(?,?);");
            ps.setString(1, user.getNome());
            ps.setString(2,user.getSenha());
            if(!ps.execute()){
                return true;
            }else{
                return true;
            }
        }catch(SQLException e){
            System.out.println("[-] Erro ao adicionar o usuário");
            System.out.println(e.getMessage());
            return false;
        }finally{
            con.fecharConexao();
        }
    }
}