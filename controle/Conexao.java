package controle;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class Conexao{
    private Connection con = null;
    public Connection getCon(){
        return this.con;
    }
    public void setCon(Connection c){
        if(c!=null){
            this.con = c;
        }
    }
    public boolean abrirConexao(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            String usuario = "root"; // Usuário do BD
            String senha = "123456"; // Senha do usuário do BD
            String banco = "banco"; // Nome do BD
            String host = "jdbc:mysql://localhost/"+banco;
            this.setCon(DriverManager.getConnection(host,usuario,senha));
            return true;
        }catch(ClassNotFoundException e){
            System.out.println("[-] Erro: Arquivo de configuração não existente");
            return false;
        }catch(SQLException e){
            System.out.println("[-] Erro ao conectar ao banco.");
            System.out.println(e.getMessage());
            return false;
        }
    }
    public boolean fecharConexao(){
        try{
            this.con.close();
            return true;
        }catch(SQLException e){
            System.out.println("[-] Conexão não pode ser fechada.");
            System.out.println(e.getMessage());
            return false;
        }
    }
}
