/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import modelo.Usuario;
import controle.UsuarioC;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;
/**
 *
 * @author clemiltonls
 */
public class Main {
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args) {
        int opcao;
        UsuarioC controle = new UsuarioC();
        Usuario user = controle.selecionarUm(2);
        if(user != null){
            System.out.println("ID do usuário: " + user.getId());
            System.out.println("Nome do usuário: " + user.getNome());
            System.out.println("Senha do usuário: " + user.getSenha());
            System.out.println("-------------------------------------------------");
            System.out.println("Deseja mudar as informações? 1 - sim | 0 - não");
            controle.removerUsuario(user.getId());
        }
        
    }

}
