# APP de Exemplo

### Como compilar
javac -d . <arquivo>

### Como executar
java -cp .:jdbc.jar Main

### Criar arquivo JAR (Executável)
#### Não esquecer de incluir o arquivo do jdbc
jar --create --file <arquivos.class> App.jar

### Editando configurações do executável

Dentro do Executável, editar o arquivo META-INF/MANIFEST.MF
Adicionar linha: 

* Main-Class: Main
* Class-Path: jdbc
